import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  private loginUrl = '/api/login'
  private registerUrl = '/api/register'

  loginUser(username, password)
  {
    return this.http.post<any>(this.loginUrl, {
      username,
      password
    });
  }

  registerUser(firstName, lastName, email, pass)
  {
    return this.http.post<any>(this.registerUrl, {
      firstName,
      lastName,
      email,
      pass
    });
  }

  getToken()
  {
    if(sessionStorage.getItem("token"))
    {
      return sessionStorage.getItem("token");
    }
    return "";
  }

  logout()
  {
    sessionStorage.clear();
  }

  public isLoggedIn()
  {
    return moment(moment().unix()).isBefore(this.getExpiration());
  }

  isLoggedOut()
  {
    return !this.isLoggedIn();
  }

  getExpiration()
  {
    const expiration = sessionStorage.getItem("token");
    if(expiration == null)
    {
      return moment(0);
    }
    const expiresAt = jwt_decode(expiration).exp;
    return moment(expiresAt);
  }
}
