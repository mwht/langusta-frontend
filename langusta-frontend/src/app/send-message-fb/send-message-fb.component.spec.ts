import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessageFbComponent } from './send-message-fb.component';

describe('SendMessageFbComponent', () => {
  let component: SendMessageFbComponent;
  let fixture: ComponentFixture<SendMessageFbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMessageFbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageFbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
