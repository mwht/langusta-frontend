import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFbPostComponent } from './add-fb-post.component';

describe('AddFbPostComponent', () => {
  let component: AddFbPostComponent;
  let fixture: ComponentFixture<AddFbPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFbPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFbPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
