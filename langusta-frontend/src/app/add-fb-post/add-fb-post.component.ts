import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-fb-post',
  templateUrl: './add-fb-post.component.html',
  styleUrls: ['./add-fb-post.component.css', '../bootstrap.css']
})
export class AddFbPostComponent implements OnInit {

  constructor(private http: HttpClient, private fb:FormBuilder) {
    this.form = this.fb.group({
      content: ['',{validators: [Validators.required], updateOn: 'blur'}],
      fbFanpage: ['',{validators: [Validators.required], updateOn: 'blur'}]
    })
   }

  ngOnInit() {
    this.getPages();
  }

  form:FormGroup;
  private fbPages = [];


  private getPages(): void
  {
    this.http.get<any>('/api/facebook/page/all').subscribe(
      res =>
      {
        if(res.state === "STATUS_SUCCESS")
        {
          this.fbPages = res.payload.accounts.data;
        }
        else
        {
          document.getElementById("info2").style.display = 'block';
        }
      },
      err =>
      {
        document.getElementById("info").style.display = 'none';
        document.getElementById("error").style.display = 'block';
        document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + err.error.additionalInfo;
      }
    )
  }

  private send(): void
  {
    const val = this.form.value;
    document.getElementById("success").style.display = 'none';
    document.getElementById("error").style.display = 'none';
    if(val.fbFanpage && val.content)
    {
      document.getElementById("info").style.display = 'block';
      const content = val.content
      this.http.post<any>('/api/facebook/page/'+val.fbFanpage+'/post', {
        content,
      }).subscribe(
        res =>
        {
          document.getElementById("info").style.display = 'none';
          if(res.state === "STATUS_SUCCESS")
          {
            document.getElementById("success").style.display = 'block';
          }
          else
          {
            document.getElementById("error").style.display = 'block';
            document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + res.additionalInfo;
          }
        },
        err =>
        {
          document.getElementById("info").style.display = 'none';
          document.getElementById("error").style.display = 'block';
          document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + err.error.additionalInfo;
        }
      )
    }
    else
    {
      document.getElementById("warning").style.display = 'block';
    }
  }

}
