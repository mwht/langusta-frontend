import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '../../node_modules/@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor{

    constructor(public auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
  {
    req = req.clone({
      setHeaders:
      {
        Authorization: 'Bearer ' + this.auth.getToken(),
        "X-Auth-Token": this.auth.getToken()
      }
    });
    return next.handle(req);
  }
}
